# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
build_table = ->
  year =  $("#tax_calculator_year").val()
  tax_rate = $("#tax_calculator_tax_rate").val()
  quote = $("#tax_calculator_quote").val()
  $.get "/tax_calculators/quote",{pop: quote,time: year, rate: tax_rate}

$ ->
  $('#tax_calculator_year,#tax_calculator_tax_rate,#tax_calculator_quote').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()