json.array!(@managers) do |manager|
  json.extract! manager, :id, :employee_name, :sale_id
  json.url manager_url(manager, format: :json)
end
