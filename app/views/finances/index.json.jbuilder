json.array!(@finances) do |finance|
  json.extract! finance, :id, :finance_quote, :tax, :year
  json.url finance_url(finance, format: :json)
end
