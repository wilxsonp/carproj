json.array!(@owners) do |owner|
  json.extract! owner, :id, :profit, :revenue, :tax
  json.url owner_url(owner, format: :json)
end
