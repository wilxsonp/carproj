json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :model, :color, :price, :vin
  json.url inventory_url(inventory, format: :json)
end
