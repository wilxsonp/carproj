class Finance < ActiveRecord::Base
  has_many :sales

  def self.calculate_quote_table(quote = 0,year = 0,tax = 0)
    Rails.logger.debug "Inputs: #{quote} #{year} #{tax}"
    quote = []
    (0..year).each do |t|
      quote << quote * Math.exp(tax * t)
    end
    return quote
  end

  def calculate_quote_table
    return self.class.calculate_quote_table(self.quote,self.year,self.tax)
  end

end
