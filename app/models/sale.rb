class Sale < ActiveRecord::Base
  belongs_to :inventory
  has_many :manager
  def self.search(search)
    if search
      self.where('name LIKE ?', "%#{search}%")
    else
      self.all
    end
  end
end
