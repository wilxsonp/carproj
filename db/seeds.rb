# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Inventory.create([{ model: 'Camry'}, {model: 'Highlander'}, {model: 'Avalon'},{ color: 'Camry'}, {color: 'Highlander'}, {color: 'Avalon'},{ vin: 'BSO35F2FRH'}, {vin: 'HSI95F46H8'}, {vin: '4F0EPG0E8I'},{ price: '25000'}, {price: '31000'}, {price: '36000'}])
Sale.create([{ customer: 'Jane Doe'}, {customer: 'Derek Cruise'}, {customer: 'John Smith'},{ quote: 'Jane Doe'}, {quote: 'Derek Cruise'}, {quote: 'John Smith'}])