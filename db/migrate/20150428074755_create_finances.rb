class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :finance_quote
      t.float :tax
      t.integer :year

      t.timestamps null: false
    end
  end
end
