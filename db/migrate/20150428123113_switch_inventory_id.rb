class SwitchInventoryId < ActiveRecord::Migration
  def change
    remove_column :inventories, :inventory_id, :integer
    add_column :sales, :inventory_id, :integer
  end
end
