class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.integer :profit
      t.integer :revenue
      t.integer :tax

      t.timestamps null: false
    end
  end
end
