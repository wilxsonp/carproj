class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :customer
      t.integer :quote
      t.string :sold
      t.integer :sale_id

      t.timestamps null: false
    end
  end
end
