class CreateManagers < ActiveRecord::Migration
  def change
    create_table :managers do |t|
      t.string :employee_name
      t.integer :sale_id

      t.timestamps null: false
    end
  end
end
