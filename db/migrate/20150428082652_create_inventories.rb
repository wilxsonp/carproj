class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :model
      t.string :color
      t.integer :price
      t.string :vin

      t.timestamps null: false
    end
  end
end
